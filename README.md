# learn-bevy


Learning Rust with the [Rust book](https://doc.rust-lang.org/book/) is fine.
Now we get serious and learn by doing a game 
following those great [tutorails](https://www.youtube.com/watch?v=TQt-v_bFdao) from [Jacques](https://www.youtube.com/@jacques-dev).

Assets from [kenney](https://kenney.nl/).

## build and run

```
# dependencies (run once)
sudo apt-get install libsdl2-dev

# update rust env
rustup update

# build and run
cargo run
```
